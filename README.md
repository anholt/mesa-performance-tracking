<div align="center">

# Mesa performance tracking
**Performance tracking** displayed in Grafana

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffffff)
![Grafana](https://img.shields.io/badge/grafana-%23F46800.svg?style=for-the-badge&logo=grafana&logoColor=white)


<img src="img/overall_mesa.gif"/>

</div>

## Table Of Content

- [Introduction](#introduction)
- [Testing](#testing)
  - [Requirements](#requirements)
  - [Running tests locally](#running-tests-locally)
    - [TLDR](#tldr)
    - [Step-by-step](#step-by-step)
- [Dashboard deployment](#dashboard-deployment)
  - [Updating dashboards](#updating-dashboards)
  - [Creating dashboards](#creating-dashboards)


## Introduction

The scripts in this repository gather data on the performance of Mesa drivers and Mesa's CI and display it in Grafana.

It is composed of three parts:

- Scripts for the data acquisition and storage in InfluxDB
- Definition of Grafana dashboards in YAML
- A gitlab-ci pipeline for running these scripts periodically and keeping the dashboards in Grafana up-to-date

After changes in production, the updated dashboards can be seen at:

- https://grafana.freedesktop.org/d/uINdWI3Mz/mesa-driver-performance
- https://grafana.freedesktop.org/d/-UCa0si7z/mesa-ci-stats
- https://ci-stats-grafana.freedesktop.org/d/Ae_TLIwVk/mesa-ci-quality-false-positives

## Testing

### Requirements
- `docker-compose` 3.7+ or `podman-compose`
- `podman` or `docker` 18.06.0+
- `git`
- `python` 3.8+
- `python3-pip`
- `python3-dev`
- `gcc`
- `libkrb5-dev` for `grafana-dashboard-builder` python requirement
- `musl-dev` if using Alpine OS

### Running tests locally

#### TLDR

- Goto your fork, Settings -> CI/CD -> Runners and get the RUNNER_REGISTRATION_TOKEN
- Run replacing the token value.
   ```
   export RUNNER_REGISTRATION_TOKEN="???"
   ./bootstrap_local_containers.sh
   ```
- Run a pipeline via Gitlab and check for the dashboards at
http://localhost:3000 and the data sources at http://localhost:8086.

#### Step-by-step

A Docker compose definition is provided so contributors can test their changes locally before submitting them for review.

Steps:

1. Install python requirements with `pip install -r requirements.txt`
1. Create a personal access token for your [user](https://gitlab.freedesktop.org/-/profile/personal_access_tokens) with the API scope and set it as a masked CI/CD variable in your forked repo with the name `GITLAB_TOKEN`
1. Disable shared runners in the user's fork repo
1. Set GRAFANA_URL=http://admin:admin@grafana:3000 as a CI/CD variable in your forked repo
1. To run the locally rootless version, one should run a rootless API daemon service via: `podman system service --time 0 &`. **If you opt to run in docker+sudo, please remember to add it accordingly from the rest of this document**.
1. Create a docker external volumes to persist data without attaching them to the docker-compose with:
   ```
   # change `podman` to `sudo docker` if using docker
   podman volume create influxdb-data  # InfluxDB
   podman volume create dashboards-data  # Grafana
   ```
1. Disable shared runners in the user's fork repo

1. Build and run the containers containing Grafana, InfluxDB and gitlab-runner, specifying the runner registration token for your fork. To accomplish that, run `docker-compose` pointing to the rootless API and overriding the rootless options.
   ```
   RUNNER_REGISTRATION_TOKEN=[enter your token here] \
      podman-compose \
         -f docker-compose.yaml \
         up
   ```
1. Or, with docker+sudo
   ```
   RUNNER_REGISTRATION_TOKEN=[enter your token here] \
      sudo -E docker-compose \
         -f docker-compose.yaml \
         up
   ```
1. Check that you can log into the Grafana service by going to http://localhost:3000 and using the [default admin account](https://grafana.com/docs/grafana/latest/administration/configuration/#admin_user).
1. Check that you can log into the InfluxDB service by going to http://localhost:8086 and using the username and password from `docker-compose.yaml` file.
1. If you want to run scheduled pipelines, you can set the RUN_THIS_STAGE_ONLY CI/CD variable for the following use cases:
    - RUN_THIS_STAGE_ONLY=send-reports  # To run only report generation and publishing
    - RUN_THIS_STAGE_ONLY=trigger-jobs  # To only run trigger-jobs job
    - RUN_THIS_STAGE_ONLY=dashboards    # To update dashboards and monitoring data
    - RUN_THIS_STAGE_ONLY=test          # To only run lint and test jobs
1. To set arguments for each script, you can set the below CI/CD variable
    - MESA_CI_STATUS_ARGS - See mesa-ci-status/mesa_ci_status.py for the argument list
    - PERFORMANCE_ALERT_ARGS - See alerts/ministat_runner.py for the argument list
1. Execute a pipeline in your fork
1. After the pipeline finishes, test your changes in http://localhost:3000

## Dashboard deployment

We are deprecating `grafana-dashboard-builder` in favor of `dashboard-exporter`.
It supports creating and editing dashboards as a code.

Currently, only the "Mesa Driver Performance" dashboard is using the new tool.
Eventually, the other ones will migrate to the new one as well.

### Updating dashboards

1. Update dashboard title, datasource name, dashboard uid etc by editing
`dashboards/metadata.yml`
2. Update the JSON model by overwriting an existing JSON file inside
`./dashboards` and ensure that it is listed in `metadata.yml`

### Creating dashboards
1. Open any dashboard in fd.o [Grafana instance](https://ci-stats-grafana.freedesktop.org).
1. Copy the JSON model to a file and put it in the `./dashboards` folder
   - You have to be an admin or the dashboard's author
   - You can find it by appending: `&editview=dashboard_json` to the dashboard URL
1. Update `dashboards/metadata.yml` the same way as the other dashboards
   - Example:
   ```
   dashboard_title:
    json_model: model.json

    # overrides are optional
    # it is more used for migrating data easily
    overrides:
      uid: myid
      datasource:
        uid: mydb
   ```
1. Run the dashboard exporter script:
   ```
   PYTHONPATH=. dashboard_exporter/exporter.py create_or_update dashboards/metadata.yml
   ```
1. Check if the new dashboard appears into the dashboards General folder, which
is available at `/dashboards` endpoint
