#!/bin/bash

set -ex

# Helper function to get current git remote url
# If it is a git url, transform it into https
# If it is a https url, return it
# If it is not a git repo, return empty string
function get_git_remote_url() ( 
    local git_remote_url
    git_remote_url=$(git remote get-url "$(git remote | head -n1)")
    if [[ "${git_remote_url}" == git@* ]]; then
        git_remote_url=$(echo "${git_remote_url}" | sed 's/^git@/https:\/\//g' | sed 's/:/\//2')
    fi
    echo "${git_remote_url%.git}"
)

warning() ( 
    # orange color
    echo -e "\033[33m$1\033[0m"
)

info() ( 
    # green color
    echo -e "\033[32m$1\033[0m"
)

error() ( 
    # red color
    echo -e "\033[31m$1\033[0m"
    exit 1
)

if [ -z "$RUNNER_REGISTRATION_TOKEN" ]; then
    error "Please set the RUNNER_REGISTRATION_TOKEN variable with your runner token"
fi

if [ -z "$DOCKER_EXEC" ]; then
    if command -v podman &> /dev/null; then
        DOCKER_EXEC="podman"
        DOCKER_COMPOSE_EXEC="podman-compose"
        SOCK_PATH="${XDG_RUNTIME_DIR}/podman/podman.sock"
    elif command -v docker &> /dev/null; then
        DOCKER_EXEC="sudo docker"
        DOCKER_COMPOSE_EXEC="docker-compose"
        SOCK_PATH="/var/run/docker.sock"
    else
        error "Please install podman or docker"
    fi
    # Used by docker-compose gitlab-runner service
    export DOCKER_SOCK_VOLUME="${SOCK_PATH}:/var/run/docker.sock:Z"
fi

if command -v "${DOCKER_COMPOSE_EXEC}" &> /dev/null; then
    info "Using ${DOCKER_COMPOSE_EXEC}..."
else
    error "Please install ${DOCKER_COMPOSE_EXEC}"
fi

GIT_CURRENT_URL=$(get_git_remote_url)

# Create docker external volumes
info "Creating Docker external volumes..."
for volume_name in influxdb-data dashboards-data; do
    if ${DOCKER_EXEC} volume ls | grep -q ${volume_name}; then
        info "Volume ${volume_name} already exists"
        continue
    fi
    ${DOCKER_EXEC} volume create ${volume_name}
done

if [ "$DOCKER_EXEC" = "podman" ]; then
    # Run a rootless API daemon service
    info "Running a rootless API daemon service..."
    ${DOCKER_EXEC} system service --time 0 &

    # Build and run containers
    info "Building and running containers in rootless environment..."
    ${DOCKER_COMPOSE_EXEC} up -d
else
    # Build and run containers
    info "Building and running containers..."
    # shellcheck disable=SC2086
    sudo -E ${DOCKER_COMPOSE_EXEC} up -d
fi

set +x
# Manual steps for setting GITLAB_TOKEN
warning "Please manually create a personal access token (https://gitlab.freedesktop.org/-/profile/personal_access_tokens) for your user with the API scope and set it as a masked CI/CD variable in your forked repo with the name GITLAB_TOKEN"

# Manual steps for disabling shared runners in the user's fork repo
warning "Please manually disable shared runners in your user's fork repo. See ${GIT_CURRENT_URL}/-/settings/ci_cd -> Runners for more information"

# Manual steps for setting variables for grafana url
warning "Please manually set GRAFANA_URL=http://admin:admin@grafana:3000 as a CI/CD variable in your forked repo"

# Manual steps for setting variables for reports generation
warning "Please manually set the following variables in your fork repo to run scheduled pipelines:"
warning "RUN_THIS_STAGE_ONLY=send-reports to run only report generation and publishing"
warning "RUN_THIS_STAGE_ONLY=trigger-jobs to only run trigger-jobs job"
warning "RUN_THIS_STAGE_ONLY=dashboards to update dashboards and monitoring data"
warning "RUN_THIS_STAGE_ONLY=test to run only lint and test jobs"
warning "See $GIT_CURRENT_URL/-/settings/ci_cd - Variables for more information"

# Manual steps for executing a pipeline in your fork
warning "Please manually execute a pipeline in your fork."

# Manual steps for testing changes
warning "After the pipeline finishes, you can test your changes in http://localhost:3000"

# Manual steps for login into Grafana and InfluxDB services
warning "Please manually login into Grafana service by going to http://localhost:3000 and using the default admin account."
warning "Also, login into InfluxDB service by going to http://localhost:8086"
warning "The credentials for both services can be found inside the docker-compose.yaml file."

