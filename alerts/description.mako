Motivation
==========

It is hard to track driver performances in Mesa CI since we have been monitoring
dozens of them in freedesktop [Grafana
instance](https://grafana.freedesktop.org/d/aH__CPd7z/mesa-driver-performance?orgId=1).

This issue will create alerts daily to keep developers aware of performance
changes in drivers being monitored.

Methodology
===========

A daily script runs
[ministat](https://www.freebsd.org/cgi/man.cgi?query=ministat) binary against
two groups of data from a data range from ${data_range_days} days before to the
execution date, looking for **${before_points}** commit measurements for the
past group, called `Before`, and **[${min_after_points}, ${max_after_points}]**
for the current group, labeled `After`. The latter is calculated dynamically
depending on the variance using the coefficient of variation formula from
[Lehr's rule of
thumb](https://rstudio-pubs-static.s3.amazonaws.com/201750_c17bc51d8553452d997ba4d258b0249f.html#sample-size).

For the curious, the underlying statistical method that `ministat` uses is the
hypothesis test. This
[article](https://towardsdatascience.com/statistical-significance-hypothesis-testing-the-normal-curve-and-p-values-93274fa32687)
explains it briefly.

Notes
-----

- Confidence Level: **${confidence_level}%**
- Absolute relative changes lesser than **1%** are ignored.
- Average values are omitted.
- Jobs and traces without relevant changes are ommitted, if all data fall in
this category the daily report will be skipped, to avoid bothering users.

See also
========

List of currently monitored drivers and traces
----------------------------------------------

<details>
<summary>Traces</summary>
<ul>
    % for trace in traces:
        <li>${trace}</li>
    % endfor
</ul>
</details>

<details>
<summary>Driver Jobs</summary>
<ul>
    % for job in jobs:
        <li>${job}</li>
    % endfor
</ul>
</details>
