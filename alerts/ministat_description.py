from __future__ import annotations

from dataclasses import dataclass
from datetime import datetime
from typing import TYPE_CHECKING
from unittest.mock import MagicMock

from alerts.templates import render_template

if TYPE_CHECKING:
    from alerts.ministat_runner import MinistatRunner


@dataclass
class MinistatDescription:
    runner: MinistatRunner

    def render_description(self, traces=None, jobs=None):
        data_range_days = self.runner.before_days
        before_points = self.runner.before_sample_size
        min_after_points = self.runner.min_after_sample_size
        max_after_points = self.runner.max_after_sample_size
        confidence_level = self.runner.confidence_level
        runner_traces, runner_jobs = self.runner.get_all_jobs_and_traces()
        traces = traces or runner_traces
        jobs = jobs or runner_jobs

        return render_template(
            "description.mako",
            update_time=datetime.now().isoformat(),
            data_range_days=data_range_days,
            before_points=before_points,
            min_after_points=min_after_points,
            max_after_points=max_after_points,
            confidence_level=confidence_level,
            traces=traces,
            jobs=jobs,
        )


if __name__ == "__main__":
    traces = [f"trace {t}" for t in range(10)]
    jobs = [f"job {t}" for t in range(10)]
    print(MinistatDescription(MagicMock()).render_description(traces, jobs))
