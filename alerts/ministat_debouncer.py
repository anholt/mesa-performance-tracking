import logging
from dataclasses import dataclass
from typing import Optional

from alerts.ministat_parser import MinistatParser
from alerts.ministat_results import MinistatResults


def num_alerts(previous_list: list) -> int:
    return len(previous_list) - previous_list.count(None)


@dataclass
class MinistatDebouncer:
    """
    Debouncing has an analogy of a table tennis ball bouncing on the ground.
    Suppose you only want to hear the first bouncing on the ground sound to
    infer that the ball is out of play. The bouncing sound would be noise to
    you.

    The same happens with alerts here; as we are doing temporal analysis, the
    system will probably detect the same detected changes from today to
    tomorrow, and the signals bounce on consecutive days. So we are prone to
    noisy and repeated alerts on successive days.

    This class will use the before and after datasets, slice them into n parts,
    to simulate what would be the results from the past and only alert when
    there is enough (not more, not less) redundant data from the recent past.
    """

    # How many commits should we look into the past
    bounce_length: int = 5
    # How many past results should be alerts to be considered a valid one
    redundancy_factor: int = 3

    def __post_init__(self):
        if self.redundancy_factor > self.bounce_length:
            raise ValueError(
                f"{self.bounce_length}  < {self.redundancy_factor}. "
                "To generate enough data for redundancy, "
                "one must use a bounce_length >= redundancy_factor."
            )

    def run(self, parser: MinistatParser, job_name, trace_name) -> Optional[MinistatResults]:
        if current := parser.run():
            # Only debounce if current data is promising
            return current if self.debounce(parser, job_name, trace_name) else None
        return None

    def debounce(self, parser: MinistatParser, job_name, trace_name):
        previous = []

        for slice_pair in self._slice_dataset(parser.before, parser.after[:-1]):
            before, after = slice_pair
            result = parser.run(before, after)
            previous.append(result)

        if num_alerts(previous) > self.redundancy_factor:
            alert_name = f"{job_name}|{trace_name}"
            logging.warning(
                f"Ignoring alert {alert_name} due to high chance of being "
                f"repeated in the last {self.bounce_length} commits."
            )
            print(previous)
            return False

        return True

    def _slice_dataset(self, left, right):
        c = left + right
        midpoint = len(left)
        # Example: slicing with window size of 3
        # before | after
        # a b c d e f g | h i j k l
        # bouncing 2 measurements
        # Last day estimation (left == 0)
        # a b c d e f | g h i
        # Last two days estimation (left == 1)
        # b c d e f g | h i j
        # Current estimation (left == 2)
        # c d e f g h | i j k
        for left_idx in range(self.bounce_length + 1):
            new_mid = midpoint + left_idx
            b = c[left_idx:new_mid]
            right = left_idx - self.bounce_length
            a = c[new_mid : right or None]  # noqa: E203

            yield b, a
