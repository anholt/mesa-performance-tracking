import logging
from pathlib import Path

from mako.template import Template, exceptions

from common.gl_log_section import GitlabSection


def render_template(filename, **kwargs):
    query_template_file = str(Path(__file__).parent / filename)
    query_template = Template(filename=query_template_file)

    try:
        query = query_template.render(**kwargs)
        with GitlabSection(
            "mako_template",
            f"Rendering {filename}: {list(kwargs.values())[:2]} template",
            start_collapsed=True,
        ):
            logging.debug(query)
        return query
    except Exception:
        logging.error(exceptions.text_error_template().render())
        raise ValueError("Could not render the template.")
