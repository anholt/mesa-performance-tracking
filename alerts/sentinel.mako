import "experimental"

start_time = experimental.addDuration(d: ${delta}, to: ${stop_time})

from(bucket: "mesa-perf-v2") 
|> range(start: start_time, stop: ${stop_time})
|> filter(fn: (r) => r._field == "commit_sha")
|> group(columns: ["trace_name", "job_name"])
|> distinct()
|> count()
