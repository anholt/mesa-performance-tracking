import argparse
import concurrent.futures
import os
import re
import sys
from collections import Counter
from datetime import datetime, timedelta
from itertools import count
from operator import itemgetter
from typing import Union

import gitlab
import yaml
from tabulate import tabulate

from common.gl_issue import gl_send_report, send_email

GITLAB_URL = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
GITLAB_PROJECT_PATH = os.environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN", "<token unspecified>")


# Accumulator base class
class CResults:
    pass


class CResultsGlObjs(CResults):
    def __init__(self):
        super().__init__()
        self.n_failed = 0
        self.n_success = 0

    def is_empty(self):
        return not self.n_failed and not self.n_success

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        obj.n_failed = self.n_failed + other.n_failed
        obj.n_success = self.n_success + other.n_success
        return obj

    def __str__(self):
        return f"{type(self)}: n_failed: {self.n_failed}, n_success: {self.n_success}"  # noqa: E501


class CResultsJobsExtraInfo(CResults):
    def __init__(self):
        super().__init__()
        # dictionary in the format {error name: counter}
        self.categories_counter = Counter()
        # dictionary in the format {test name: counter}
        self.test_error_counter = Counter()
        # dictionary in the format {job_id: error_reason}
        self.error_categories = {}
        # dictionary in the format {job_id: error_test_name}
        self.error_test_name = {}

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        # sum the values with same keys
        obj.categories_counter = self.categories_counter + other.categories_counter
        obj.test_error_counter = self.test_error_counter + other.test_error_counter
        obj.error_categories = self.error_categories | other.error_categories
        obj.error_test_name = self.error_test_name | other.error_test_name
        return obj


class CResultsJobs(CResultsGlObjs):
    def __init__(self):
        super().__init__()
        self.pipelines = Counter()
        self.names_fail = Counter()
        self.names_success = Counter()
        self.failed_jobs = []
        # dict key has the format f'{job.pipeline["id"]}_{job.stage}'
        self.stages_more30min: dict[str, gitlab.ProjectPipelineJob] = {}
        self.compute_extra_info = CResultsJobsExtraInfo()
        self.n_jobs_per_stage = Counter()
        self.flakeness = {}

    def __add__(self, other):
        obj = super().__add__(other)
        # sum the values with same keys
        obj.pipelines = self.pipelines + other.pipelines
        obj.names_fail = self.names_fail + other.names_fail
        obj.names_success = self.names_success + other.names_success
        obj.failed_jobs = self.failed_jobs + other.failed_jobs
        obj.n_jobs_per_stage = self.n_jobs_per_stage + other.n_jobs_per_stage
        obj.compute_extra_info = self.compute_extra_info + other.compute_extra_info
        # shallow copy is used because the objects are read only after filled
        obj.stages_more30min = self.stages_more30min.copy()
        obj.stages_more30min.update(other.stages_more30min)
        obj.flakeness = self.flakeness.copy()
        obj.flakeness.update(other.flakeness)
        return obj

    def __str__(self):
        return f"{super().__str__()} | {str(self.pipelines)} | {str(self.names_fail)}"  # noqa: E501


class ComputeObjs:
    def __init__(self, c_results):
        self.objs: list[Union[gitlab.ProjectPipelineJob, gitlab.ProjectPipeline]] = []
        self.c_results = c_results

    def run(self):
        # call all functions starting with compute_once__
        for i in dir(self):
            func = getattr(self, i)
            if i.startswith("compute_once_") and callable(func):
                func()

        # for every obj, call all functions starting with compute_obj_
        for gl_obj in self.objs:
            for i in dir(self):
                func = getattr(self, i)
                if i.startswith("compute_obj_") and callable(func):
                    func(gl_obj)

        return self.c_results


class ComputeGlObjs(ComputeObjs):
    def __init__(self, c_results, period=None):
        super().__init__(c_results)
        self.period = period

    def filter_out(self, gl_obj):
        if not self.period:
            return False
        if (
            hasattr(gl_obj, "finished_at")
            and gl_obj.finished_at
            and gl_obj.finished_at < self.period[0]
        ):
            return True
        return bool(hasattr(gl_obj, "updated_at") and gl_obj.updated_at < self.period[0])

    def compute_obj_count_failed_gl_objs(self, gl_obj):
        if gl_obj.status == "success":
            self.c_results.n_success += 1
        elif gl_obj.status == "failed":
            self.c_results.n_failed += 1
        elif gl_obj.status not in ["canceled", "skipped", "manual"]:
            raise Exception("Unknown status ", gl_obj.status, gl_obj)


class ComputeJobsExtraInfo(ComputeObjs):
    def __init__(self, traces_config=None):
        super().__init__(CResultsJobsExtraInfo())
        self.trace = None
        self.job = None
        self.failures_csv = None
        self.junit_xml = None
        self.traces_config = traces_config

    def gitlab_request_job_extrainfo(self, project, job):
        def get_artifact(pjob, path):
            try:
                return pjob.artifact(path).decode("UTF-8")
            except gitlab.exceptions.GitlabGetError as e:
                # File doesn't exist, ignore
                if e.response_code != 404:
                    raise
            return None

        if job.status != "failed":
            return
        # We need jobs object from the project to get traces
        pjob = project.jobs.get(job.id)
        self.trace = pjob.trace().decode("UTF-8")
        self.job = job
        self.failures_csv = get_artifact(pjob, "results/failures.csv")
        # Only process junit if there is no failures.csv to avoid processing
        # errors twice. This is the case when the test didn't run through
        # deqp-runner (piglit direct run for instance)
        if not self.failures_csv:
            self.junit_xml = get_artifact(pjob, "results/junit.xml")

    def compute_once_categorize_errors(self):
        if not self.traces_config or not self.job:
            return

        for category in self.traces_config["categories"]:
            # if exclusive, do nothing if already classified
            if category.get("exclusive", True) and self.job.id in self.c_results.error_categories:
                continue

            p = "|".join(category["patterns"])
            if re.findall(p, self.trace or ""):
                name = f'[{category["tag"]}] {category["name"]}'
                # Set category only for the first match (priority order)
                if self.job.id not in self.c_results.error_categories:
                    self.c_results.error_categories[self.job.id] = category
                # Count category occurrence
                self.c_results.categories_counter.update([name])

    def test_key(self, test, job_name):
        def remove_shard_suffix(name):
            suffix = re.search(r" (\d+/\d+)$", name)
            return name.removesuffix(suffix[1]) if suffix is not None else name

        # Remove shard suffix
        job_name = remove_shard_suffix(job_name)
        return f"{job_name}|{test}|"

    def compute_once_count_deqp_failures_csv(self):
        if not self.failures_csv:
            return

        lines = self.failures_csv.splitlines()
        for line in lines:
            if test := line.split(",")[0]:
                key = self.test_key(test, self.job.name)
                self.c_results.test_error_counter.update([key])
                self.c_results.error_test_name[self.job.id] = key

    def compute_once_count_piglit_junit_xml(self):
        if not self.junit_xml:
            return

        lines = self.junit_xml.splitlines()
        for line in lines:
            res = re.search('.*name="(.+?)".*classname="(.+?)".*status="(.+?)".*', line)
            if not res:
                continue

            name, classname, status = res.groups()
            if status == "pass":
                continue

            test = f"{classname}/{name}"
            key = self.test_key(test, self.job.name)
            self.c_results.test_error_counter.update([key])
            self.c_results.error_test_name[self.job.id] = key

    def compute_once_count_skqp(self):
        if self.failures_csv or self.junit_xml or not self.trace:
            return

        # Get test names, exemples:
        # [0m2022-05-24 13:56:28.106391: Starting: gl_zero_control_stroke
        # [0m2022-05-24 13:58:35.054641: Starting test: unitTest_AbandonedContextImage  # noqa: E501
        pattern = r": Starting.*: *(.+)"
        tests = re.findall(pattern, self.trace)
        if not tests:
            return

        # Get test status, examples:
        # [0m2022-05-24 13:56:07.475938: Passed:   gl_pathopsinverse
        # [0m2022-05-24 13:55:31.365298: FAILED:   gl_atlastext (19)
        # [0m2022-05-24 13:58:35.054677: Test passed:   unitTest_AbandonedContextImage  # noqa: E501
        # Get status from lines with the test name that doesn't have
        # "Starting" word
        tests_p = "|".join(tests)
        pattern = f": ((?!Starting).*): *({tests_p})"
        res = re.findall(pattern, self.trace)

        for status, test in res:
            if "passed" in status.lower():
                continue
            key = self.test_key(test, self.job.name)
            self.c_results.test_error_counter.update([key])
            self.c_results.error_test_name[self.job.id] = key


class ComputeJobs(ComputeGlObjs):
    def __init__(self, period=None):
        super().__init__(CResultsJobs(), period)
        self.objs: list[gitlab.ProjectPipelineJob] = []

    def gitlab_request_jobs_in_pipeline_in_page(self, pipeline, page):
        list_args = {
            "page": page,
            "per_page": 100,
            "include_retried": True,
            "username": "marge-bot",
        }
        jobs = pipeline.jobs.list(**list_args)
        self.objs = [job for job in jobs if not self.filter_out(job)]
        return self.objs

    def compute_obj_count_pipelines(self, job):
        if job.status != "failed":
            return
        p_id = job.pipeline["id"]
        self.c_results.pipelines.update([p_id])

    def compute_obj_count_names(self, job):
        if job.status == "failed":
            self.c_results.names_fail.update([job.name])
        if job.status == "success":
            self.c_results.names_success.update([job.name])

    def compute_obj_list_of_failed_jobs(self, job):
        if job.status != "failed":
            return
        self.c_results.failed_jobs.append(job)

    def compute_obj_stage_duration(self, job):
        if job.status not in ["failed", "success", "canceled"]:
            return

        if job.queued_duration is None:
            return

        key = f'{job.pipeline["id"]}_{job.stage}'
        tot_duration = job.queued_duration + (job.duration or 0)

        if tot_duration < 30 * 60:
            return

        compare = self.c_results.stages_more30min.get(key)
        compare_time = compare.queued_duration + (compare.duration or 0) if compare else 0

        if compare_time < tot_duration:
            self.c_results.stages_more30min[key] = job

    def compute_obj_cnt_stages(self, job):
        key = f'{job.pipeline["id"]}_{job.stage}'
        self.c_results.n_jobs_per_stage.update([key])


class CResultsPipelinesExtraInfo(CResults):
    def __init__(self):
        super().__init__()
        # dictionary in the format {pipeline_id: mr_title}
        self.mr_info = {}
        self.pipelines_more1h = []
        self.pipelines_more30min = []

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        obj.mr_info = self.mr_info.copy()
        obj.mr_info.update(other.mr_info)
        obj.pipelines_more1h = self.pipelines_more1h + other.pipelines_more1h
        obj.pipelines_more30min = self.pipelines_more30min + other.pipelines_more30min
        return obj


# Compute object to extract information per pipeline
class ComputePipelinesExtraInfo(ComputeObjs):
    def __init__(self):
        super().__init__(CResultsPipelinesExtraInfo())

    def get_mr_name_from_ref(self, project, ref):
        mr_id = ref.split("/")[2]
        mr = project.mergerequests.get(mr_id)
        return mr.title

    def gitlab_request_pipeline_extrainfo(self, project, pipeline):
        self.c_results.mr_info[pipeline.id] = self.get_mr_name_from_ref(project, pipeline.ref)

        # fetching pipeline from the project provides more info
        pipeline = project.pipelines.get(pipeline.id)
        total_duration = (pipeline.duration or 0) + (pipeline.queued_duration or 0)
        if total_duration >= 60 * 60:
            self.c_results.pipelines_more1h.append(pipeline)
        if total_duration >= 30 * 60:
            self.c_results.pipelines_more30min.append(pipeline)


class CResultsPipelines(CResultsGlObjs):
    def __init__(self):
        super().__init__()
        self.computed_jobs = CResultsJobs()
        self.compute_extra_info = CResultsPipelinesExtraInfo()

    def __add__(self, other):
        obj = super().__add__(other)
        obj.computed_jobs = self.computed_jobs + other.computed_jobs
        obj.compute_extra_info = self.compute_extra_info + other.compute_extra_info
        return obj

    def __str__(self):
        return f"{super().__str__()} | {str(self.computed_jobs)}"


class ComputePipelines(ComputeGlObjs):
    def __init__(self, period):
        super().__init__(CResultsPipelines(), period)
        self.objs: list[gitlab.ProjectPipeline] = []

    def gitlab_request_pipelines_in_page(self, project, page):
        list_args = {
            "per_page": 100,
            "page": page,
            "updated_after": self.period[0] + "T00:00:00.000Z",
            "updated_before": self.period[1] + "T23:59:59.999Z",
            "username": "marge-bot",
            "scope": "finished",
            "source": "merge_request_event",
        }
        pipelines = project.pipelines.list(**list_args)
        self.objs = [pipeline for pipeline in pipelines if not self.filter_out(pipeline)]
        return self.objs


class CiProcessor:
    def __init__(self, project, period, traces_config):
        self.project = project
        self.period = period
        self.n_pages = 4
        self.traces_config = traces_config

    # Call function in parallel and sum the results
    def process_parallel(self, ret_type, function, *argv):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            results = executor.map(function, *argv)
        return sum(results, ret_type())

    def process_pages(self, ret_type, init_page, function_in_page, *argv):
        return self.process_parallel(
            ret_type, function_in_page, range(init_page, init_page + self.n_pages), *argv
        )

    def process_pipelines_in_page(self, page):
        compute = ComputePipelines(self.period)
        pipelines = compute.gitlab_request_pipelines_in_page(self.project, page)
        results = compute.run()
        results.computed_jobs = self.process_parallel(
            CResultsJobs, self.process_jobs_in_pipeline, pipelines
        )
        results.compute_extra_info = self.process_parallel(
            CResultsPipelinesExtraInfo, self.process_pipeline_extra_info, pipelines
        )
        return results

    def process_pipeline_extra_info(self, pipeline):
        compute = ComputePipelinesExtraInfo()
        compute.gitlab_request_pipeline_extrainfo(self.project, pipeline)
        return compute.run()

    def process_job_extra_info(self, job):
        compute = ComputeJobsExtraInfo(self.traces_config)
        compute.gitlab_request_job_extrainfo(self.project, job)
        return compute.run()

    def process_jobs_in_pipeline_in_page(self, page, pipeline):
        compute = ComputeJobs(self.period)
        jobs = compute.gitlab_request_jobs_in_pipeline_in_page(pipeline, page)
        results = compute.run()
        results.compute_extra_info = self.process_parallel(
            CResultsJobsExtraInfo, self.process_job_extra_info, jobs
        )
        return results

    def process_flakeness_in_pipeline(self, results_jobs):
        def get_failed_jobs_by_name(name):
            res = []
            for job in results_jobs.failed_jobs:
                if job.name == name:
                    res.append(job)
            return res

        flakeness = {}
        for job_name in results_jobs.names_fail:
            if job_name in results_jobs.names_success:
                flakeness[job_name] = {
                    "n_tries": results_jobs.names_success[job_name]
                    + results_jobs.names_fail[job_name],  # noqa: E131
                    "jobs": get_failed_jobs_by_name(job_name),
                }
        return flakeness

    def process_jobs_in_pipeline(self, pipeline):
        results = CResultsJobs()
        for pg in count(1, self.n_pages):
            results_pg = self.process_pages(
                CResultsJobs, pg, self.process_jobs_in_pipeline_in_page, [pipeline] * self.n_pages
            )
            results += results_pg
            if results_pg.is_empty():
                break

        if flakeness := self.process_flakeness_in_pipeline(results):
            results.flakeness[pipeline] = flakeness
        return results

    def process_pipelines(self):
        results = CResultsPipelines()
        for pg in count(1, self.n_pages):
            print(f"Processing pipelines pages {pg}..{pg + self.n_pages - 1}", file=sys.stderr)
            results_pg = self.process_pages(CResultsPipelines, pg, self.process_pipelines_in_page)
            results += results_pg
            if results_pg.is_empty():
                break
        return results


def generate_report(period, data):
    def md_link(text, link):
        max_len = 40
        if len(text) > max_len:
            text = text[:max_len] + "..."
        return f"[{text}]({link})"

    def format_timedelta(seconds):
        # remove seconds
        return ":".join(str(timedelta(seconds=seconds)).split(":")[:2])

    def format_job_list(job_list):
        res = ""
        for job in job_list:
            tag = error_categories.get(job.id, {"tag": "unknown"})["tag"]
            if res:
                res = f"{res}, "
            res = f"{res} {md_link(str(job.id), job.web_url)} ({tag})"
        return res

    error_categories = data.computed_jobs.compute_extra_info.error_categories
    flakeness = data.computed_jobs.flakeness
    tabulated_flakeness_header = ["pipeline", "job name", "n_tries", "jobs"]

    tabulated_flakeness = []
    flake_job_list = []
    for pipeline in flakeness:
        for job_name, job_info in flakeness[pipeline].items():
            flake_job_list += job_info["jobs"]
            tabulated_flakeness.append(
                [
                    md_link(str(pipeline.id), pipeline.web_url),
                    job_name,
                    job_info["n_tries"],
                    format_job_list(job_info["jobs"]),
                ]
            )
    tabulated_flakeness = sorted(tabulated_flakeness, key=itemgetter(1))
    tabulated_flakeness_str = tabulate(
        tabulated_flakeness, tabulated_flakeness_header, tablefmt="pipe"
    )

    # flake test counter
    error_test_name = data.computed_jobs.compute_extra_info.error_test_name
    jobs_by_error = {}
    flake_test_error_counter = Counter()
    for flake_job in flake_job_list:
        if flake_job.id not in error_test_name:
            continue
        test_name = error_test_name[flake_job.id]
        flake_test_error_counter.update([test_name])
        jobs_by_error[test_name] = jobs_by_error.get(test_name, [])
        jobs_by_error[test_name].append(flake_job)

    tabulated_flake_test_error_header = ["job", "test", "number of errors", "jobs"]
    tabulated_flake_test_error = sorted(
        flake_test_error_counter.items(), key=itemgetter(1), reverse=True
    )[:10]
    tabulated_flake_test_error = (
        [t.split("|")[0], t.split("|")[1], n, format_job_list(jobs_by_error[t])]
        for t, n in tabulated_flake_test_error
    )
    tabulated_flake_test_error_str = tabulate(
        tabulated_flake_test_error, tabulated_flake_test_error_header, tablefmt="pipe"
    )

    # calculate number of pipeline failures from jobs
    n_pipe_failures = len(data.computed_jobs.pipelines)
    # Total calculated from the pipeline
    n_pipe_total = data.n_failed + data.n_success
    pipe_failing_rate = (100 * n_pipe_failures / n_pipe_total) if n_pipe_total else 0

    n_jobs_failures = data.computed_jobs.n_failed
    n_jobs_total = n_jobs_failures + data.computed_jobs.n_success
    jobs_failing_rate = (100 * n_jobs_failures / n_jobs_total) if n_jobs_total else 0

    # Top 10 failing jobs for format [(name, n_failures), (),]
    tabulated_top5_jobs_header = ["job name", "number of failures"]
    tabulated_top5_jobs = sorted(
        data.computed_jobs.names_fail.items(), key=itemgetter(1), reverse=True
    )[:10]
    tabulated_top5_jobs_str = tabulate(
        tabulated_top5_jobs, tabulated_top5_jobs_header, tablefmt="pipe"
    )

    # categories of errors
    tabulated_categories_header = ["error category", "number of occurrences"]
    categories_counter = data.computed_jobs.compute_extra_info.categories_counter
    tabulated_categories = sorted(categories_counter.items(), key=itemgetter(1), reverse=True)[:10]
    tabulated_categories_str = tabulate(
        tabulated_categories, tabulated_categories_header, tablefmt="pipe"
    )

    # test error counter

    tabulated_test_error_header = ["job", "test", "number of errors"]
    test_error_counter = data.computed_jobs.compute_extra_info.test_error_counter
    tabulated_test_error = sorted(test_error_counter.items(), key=itemgetter(1), reverse=True)[:10]
    tabulated_test_error = ([t.split("|")[0], t.split("|")[1], n] for t, n in tabulated_test_error)
    tabulated_test_error_str = tabulate(
        tabulated_test_error, tabulated_test_error_header, tablefmt="pipe"
    )

    tabulated_failed_jobs = []
    tabulated_failed_jobs_header = ["job", "pipeline", "stage", "tag", "error category"]

    # Limited to 100 jobs
    mr_names = data.compute_extra_info.mr_info
    tabulated_failed_jobs.extend(
        [
            md_link(job.name, job.web_url),
            md_link(mr_names[job.pipeline["id"]], job.pipeline["web_url"]),
            job.stage,
            error_categories.get(job.id, {"tag": ""})["tag"],
            error_categories.get(job.id, {"name": ""})["name"],
        ]
        for job in data.computed_jobs.failed_jobs[:100]
    )
    tabulated_failed_jobs_str = tabulate(
        tabulated_failed_jobs, tabulated_failed_jobs_header, tablefmt="pipe"
    )

    if period[0] == period[1]:
        dt = datetime.strptime(period[0], "%Y-%m-%d")
        report_period = f"Daily report for **{dt.strftime('%a %b %d %Y')}**"
    else:
        report_period = f"**{period[0]} - {period[1]}**"

    pipelines_more1h = data.compute_extra_info.pipelines_more1h
    n_pipelines_more1h = len(pipelines_more1h)
    pipelines_more1h_rate = (100 * n_pipelines_more1h / n_pipe_total) if n_pipe_total else 0

    pipelines_more30min = data.compute_extra_info.pipelines_more30min
    n_pipelines_more30min = len(pipelines_more30min)
    pipelines_more30min_rate = (100 * n_pipelines_more30min / n_pipe_total) if n_pipe_total else 0

    tabulated_pipelines_more30min_header = [
        "pipeline",
        "queued duration",
        "exec duration",
        "total duration",
    ]
    sorted_pipelines_more30min = sorted(
        pipelines_more30min, key=lambda kv: kv.queued_duration + kv.duration, reverse=True
    )[:10]
    tabulated_pipelines_more30min = (
        [
            md_link(mr_names[pipeline.id], pipeline.web_url),
            format_timedelta(pipeline.queued_duration),
            format_timedelta(pipeline.duration),
            format_timedelta(pipeline.queued_duration + pipeline.duration),
        ]
        for pipeline in sorted_pipelines_more30min
    )
    tabulated_pipelines_more30min_str = tabulate(
        tabulated_pipelines_more30min, tabulated_pipelines_more30min_header, tablefmt="pipe"
    )

    stages_more30min = data.computed_jobs.stages_more30min
    n_jobs_per_stage = data.computed_jobs.n_jobs_per_stage
    n_stages_more30min = len(stages_more30min.keys())
    n_stages_total = len(n_jobs_per_stage.keys())
    stages_more30min_rate = (100 * n_stages_more30min / n_stages_total) if n_stages_total else 0

    tabulated_stages_more30min_header = [
        "stage",
        "longest job",
        "pipeline",
        "queued duration",
        "exec duration",
        "total duration",
    ]
    sorted_stages_more30min = sorted(
        stages_more30min.items(),
        key=lambda kv: kv[1].queued_duration + (kv[1].duration or 0),
        reverse=True,
    )[:10]
    tabulated_stages_more30min = (
        [
            job.stage,
            md_link(job.name, job.web_url),
            md_link(mr_names[job.pipeline["id"]], job.pipeline["web_url"]),
            format_timedelta(job.queued_duration),
            format_timedelta(job.duration or 0),
            format_timedelta(job.queued_duration + (job.duration or 0)),
        ]
        for _, job in sorted_stages_more30min
    )
    tabulated_stages_more30min_str = tabulate(
        tabulated_stages_more30min, tabulated_stages_more30min_header, tablefmt="pipe"
    )

    return f"""
## SUMMARY:

PERIOD: {report_period}

FAILED MERGE PIPELINES: **{n_pipe_failures}/{n_pipe_total} - {pipe_failing_rate:.2f}%**

FAILED JOBS: **{n_jobs_failures}/{n_jobs_total} - {jobs_failing_rate:.2f}%**

PIPELINES TAKING MORE THAN 1H: **{n_pipelines_more1h}/{n_pipe_total} - {pipelines_more1h_rate:.2f}%**

PIPELINES TAKING MORE THAN 30MIN: **{n_pipelines_more30min}/{n_pipe_total} - {pipelines_more30min_rate:.2f}%**

STAGES TAKING MORE THAN 30MIN: **{n_stages_more30min}/{n_stages_total} - {stages_more30min_rate:.2f}%**

## DETAILED:

### CI LEVEL FLAKES:

{tabulated_flakeness_str}

### FLAKES ERRORS PER TESTS (TOP 10):

{tabulated_flake_test_error_str}

### TOP 10 FAILING JOBS:

{tabulated_top5_jobs_str}

### ERRORS PER CATEGORY (TOP 10):

{tabulated_categories_str}

### FAILED JOBS (limited to 100 entries):

{tabulated_failed_jobs_str}

### ERRORS PER TESTS (TOP 10):

{tabulated_test_error_str}

### PIPELINES TAKING MORE THAN 30MIN (TOP 10):

{tabulated_pipelines_more30min_str}

### STAGES TAKING MORE THAN 30min (TOP 10):

{tabulated_stages_more30min_str}

## INFORMATION:
<details><summary>details</summary>

  - All numbers only considers **{GITLAB_PROJECT_PATH}**.
  - Pipelines with updated_at **< {period[0]}** and jobs with finished_at **< {period[0]}** were ignored.
  - FAILED MERGE PIPELINES:
      - Calculated from finished pipelines triggered by Marge in a merge requests.
      - Counted when a pipeline contains a failed job, even if it got retried and the pipeline passed (status success).
  - FAILED JOBS: Consider jobs from the same pipelines from FAILED MERGE PIPELINES.
  - PIPELINES TAKING MORE THAN 30MIN also counts PIPELINES TAKING MORE THAN 1H.
  - STAGES TAKING MORE THAN 30MIN: same stages from different pipelines are counted as different stages.
  - Flakes are detected if there was at least one fail and one success job with the same name on the pipeline.
</details>
"""  # noqa: E501


def main(period, email_to, gitlab_post, traces_config):
    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN, retry_transient_errors=True)
    gl.auth()
    project = gl.projects.get(GITLAB_PROJECT_PATH)
    compute = CiProcessor(project, period, traces_config)
    results = compute.process_pipelines()
    report = generate_report(period, results)
    print(report)
    report_title = "Mesa CI Daily Report"
    send_email(email_to, report, report_title)
    if gitlab_post:
        first_month_day = period[1][:-2] + "01"
        gl_send_report(project, report, report_title, first_month_day, "CI daily")


if __name__ == "__main__":
    yesterday = datetime.strftime(datetime.utcnow() - timedelta(1), "%Y-%m-%d")
    parser = argparse.ArgumentParser()
    parser.add_argument("-df", "--datefrom", help="Starting date XXXX-XX-XX", default=yesterday)
    parser.add_argument("-du", "--dateuntil", help="End date XXXX-XX-XX", default=yesterday)
    parser.add_argument(
        "-et", "--emailto", default="", help="Comma separated emails to send the report to"
    )
    parser.add_argument(
        "-gp",
        "--gitlab_post",
        action="store_true",
        help=(
            "Post report to gitlab issue with label 'CI daily'. "
            "Create one if no such issue exist for [--dateuntil] month"
        ),
    )  # noqa: E501
    parser.add_argument(
        "-cf",
        "--config",
        default="mesa-ci-status/traces_config.yaml",
        help="Yaml configuration file (where errors categories are described)",
    )  # noqa: E501
    parser.add_argument("-ld", "--lastdays", type=int, help="Generate report for the last X days")
    args = parser.parse_args()

    if args.lastdays:
        args.datefrom = datetime.strftime(datetime.utcnow() - timedelta(args.lastdays), "%Y-%m-%d")

    with open(args.config) as f:
        traces_config = yaml.load(f, Loader=yaml.FullLoader)
    main((args.datefrom, args.dateuntil), args.emailto, args.gitlab_post, traces_config)
