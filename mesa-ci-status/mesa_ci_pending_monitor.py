import argparse
import os
from datetime import datetime, timedelta
from functools import reduce

import gitlab
from tabulate import tabulate
from tinydb import Query, TinyDB

from common.gl_issue import gl_send_report, send_email

GITLAB_URL = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
GITLAB_PROJECT_PATH = os.environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN", "<token unspecified>")


def alert_to_be_sent(db, job):
    alerts = [
        ("a day", 24 * 60 * 60),
        ("6 hours", 6 * 60 * 60),
        ("1 hour", 60 * 60),
        ("30 minutes", 30 * 60),
    ]
    for alert, duration in alerts:
        if job.queued_duration >= duration:
            return alert
    return None


def is_alert_already_sent(db, job, alert):
    qjob = Query()
    state = db.search((qjob.id == job.id) & (qjob.alert_sent[alert].exists()))
    return True if state else False


def analyse_job(db, job, alerts_to_be_sent):
    alert = alert_to_be_sent(db, job)
    if not alert or is_alert_already_sent(db, job, alert):
        return
    alerts_to_be_sent.append((job, alert))


def get_alerts_to_be_sent(db, jobs):
    alerts_to_be_sent = []
    for job in jobs:
        analyse_job(db, job, alerts_to_be_sent)
    return alerts_to_be_sent


def generate_report(alerts_to_be_sent, pending_jobs):
    def md_link(text, link):
        max_len = 40
        if len(text) > max_len:
            text = text[:max_len] + "..."
        return f"[{text}]({link})"

    def format_timedelta(seconds):
        # remove seconds
        return ":".join(str(timedelta(seconds=seconds)).split(":")[:2])

    msg = ""
    for job, alert in alerts_to_be_sent:
        job_name = md_link(job.name, job.web_url)
        msg = f"{msg}\n* Job {job_name} is pending for more than {alert} already. Please check."  # noqa: E501

    tabulated_pending_jobs_header = ["job", "stage", "queued duration"]
    tabulated_pending_jobs = (
        [md_link(job.name, job.web_url), job.stage, format_timedelta(job.queued_duration)]
        for job in pending_jobs
    )
    tabulated_pending_jobs = sorted(tabulated_pending_jobs, key=lambda j: j[2], reverse=True)
    tabulated_pending_jobs_str = tabulate(
        tabulated_pending_jobs, tabulated_pending_jobs_header, tablefmt="pipe"
    )
    return f"""{msg}

List of current pending jobs:

{tabulated_pending_jobs_str}
"""


def db_set_alerts_as_sent(db, alerts):
    qjob = Query()
    time_now = str(datetime.utcnow())

    for job, alert in alerts:
        if db.contains(qjob.id == job.id):
            db.update({"alert_sent": {alert: time_now}}, qjob.id == job.id)
        else:
            db.insert({"id": job.id, "alert_sent": {alert: time_now}})


def db_delete_non_pending_jobs(db, pending_jobs):
    if not pending_jobs:
        db.truncate()
        return

    qjob = Query()
    q = [qjob.id != job.id for job in pending_jobs]
    q = reduce((lambda x, y: x & y), q)
    db.remove(q)


def main(db_file, email_to, gitlab_post):
    db = TinyDB(db_file)

    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN, retry_transient_errors=True)
    gl.auth()
    project = gl.projects.get(GITLAB_PROJECT_PATH)

    pending_jobs = project.jobs.list(scope="pending")
    filtered = list(filter(lambda job: job.pipeline["status"] != "manual", pending_jobs))
    alerts = get_alerts_to_be_sent(db, filtered)
    if alerts:
        report = generate_report(alerts, pending_jobs)
        print(report)
        report_title = "Mesa CI: pending job alert"
        send_email(email_to, report, report_title)
        db_set_alerts_as_sent(db, alerts)
        if gitlab_post:
            first_month_day = str(datetime.utcnow().date().replace(day=1))
            gl_send_report(project, report, report_title, first_month_day, "CI alert")
    else:
        print("No alerts were generated.")
    db_delete_non_pending_jobs(db, pending_jobs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-db",
        "--database",
        help="Database file for saving job states",
        default="mesa_ci_pending_monitor.db",
    )
    parser.add_argument(
        "-et", "--emailto", default="", help="Comma separated emails to send the report to"
    )
    parser.add_argument(
        "-gp",
        "--gitlab_post",
        action="store_true",
        help=(
            "Post report to gitlab issue with label 'CI daily'. "
            "Create one if no such issue exist for [--dateuntil] month"
        ),
    )  # noqa: E501
    args = parser.parse_args()
    main(args.database, args.emailto, args.gitlab_post)
