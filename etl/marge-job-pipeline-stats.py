#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import logging
from dataclasses import dataclass, fields
from functools import partial
from typing import Any, Tuple

from gitlab.v4.objects.pipelines import ProjectPipeline, ProjectPipelineJob
from influxdb_client import WriteApi
from wrappers.gitlab_connector import GitlabConnector
from wrappers.influxdb_helper import InfluxdbSource

from common.thread_utils import SmartThreadPoolExecutor


# base dataclass to enforce typing
@dataclass(frozen=True)
class ForcedType:
    """Converts primitive typed attributes automatically"""

    @staticmethod
    def is_complex(obj):
        return hasattr(obj, "__dict__")

    def __post_init__(self):
        # created_at = datetime.fromisoformat(self.created_at.rstrip("Z"))
        # object.__setattr__(self, "created_at", created_at)
        for field in fields(self):
            new_value = field.default or None
            if value := getattr(self, field.name):
                if ForcedType.is_complex(value):
                    continue
                new_value = field.type(value)

            object.__setattr__(self, field.name, new_value)


@dataclass(frozen=True)
class PipelinePoint(ForcedType):
    id: int
    created_at: str
    status: str
    sha: str


@dataclass(frozen=True)
class JobPoint(ForcedType):
    id: int
    created_at: str
    pipeline_id: int
    status: str
    stage: str
    name: str
    duration: float = -1.0


def from_object_to_data_obj(klass, object, attributes: Tuple, prefix: str = "") -> Any:
    kwargs = {prefix + attr: getattr(object, attr) for attr in attributes}
    return klass(**kwargs)


def update_job(writer_api: WriteApi, job: ProjectPipelineJob) -> None:
    tags: tuple = ("status", "stage", "name")
    fields: tuple = ("pipeline_id", "duration", "id", "created_at")
    job_attrs = tuple(tags + fields)
    job_object = from_object_to_data_obj(JobPoint, job, job_attrs)
    writer_api.write(
        bucket="marge-stats",
        record=job_object,
        record_time_key="created_at",
        record_measurement_name="job",
        record_tag_keys=tags,
        record_field_keys=fields,
    )


def update_job_data(pipeline: ProjectPipeline, writer_api: WriteApi) -> None:
    job: ProjectPipelineJob
    for job in pipeline.jobs.list(iterator=True, include_retried=True):  # type: ignore
        update_job(writer_api, job)


def upload_pipeline_data(pipeline: ProjectPipeline, writer_api: WriteApi) -> None:
    tags: tuple = ("status",)
    fields: tuple = ("id", "created_at", "sha")
    pipeline_attrs = tuple(tags + fields)
    pipeline_obj: PipelinePoint = from_object_to_data_obj(PipelinePoint, pipeline, pipeline_attrs)
    logging.info(f"Pipeline: {pipeline_obj}")
    writer_api.write(
        bucket="marge-stats",
        record=pipeline_obj,
        record_time_key="created_at",
        record_measurement_name="pipeline",
        record_tag_keys=tags,
        record_field_keys=fields,
    )


def upload_data(gl: GitlabConnector, writer_api, pipeline) -> None:
    upload_pipeline_data(pipeline, writer_api)
    update_job_data(pipeline, writer_api)


def main():
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    # Log thread name and date
    logging.basicConfig(
        format="%(asctime)s %(threadName)s %(levelname)s %(message)s",
        level=logging.INFO,
    )

    source = InfluxdbSource("marge-stats")
    gl = GitlabConnector()

    last_write = source.get_last_write(measurement="pipeline", field="id")
    # Uncomment below to force data extraction since the specified start range.
    # last_write = source.get_start_date()
    # from datetime import datetime, timedelta
    # last_write = datetime.now() - timedelta(days=20)

    logging.info(f"Analyzing data from {last_write} to now.")

    with source.writer_instance() as writer_api:
        # Run serialized: uncomment below to run sequentially
        # for pipeline in gl.pipeline_list(last_write, source="merge_request_event"):
        #     upload_data(gl, writer_api, pipeline)
        # Run serialized: comment the rest of block below to run sequentially

        upload_pipeline = partial(upload_data, gl, writer_api)
        with SmartThreadPoolExecutor() as executor:
            list(
                executor.map(
                    upload_pipeline,
                    gl.pipeline_list(last_write, source="merge_request_event"),
                )
            )


if __name__ == "__main__":
    main()
