#!/usr/bin/python3

# Copyright © 2023 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import json
import os
import re
import threading
from datetime import datetime, timedelta
from functools import partial
from typing import Any, Dict, List
from xml.etree import ElementTree as ET

import yaml
from gitlab.v4.objects import (
    ProjectJob,
    ProjectMergeRequest,
    ProjectPipeline,
    ProjectPipelineJob,
)
from influxdb_client import Point
from wrappers.gitlab_connector import GitlabConnector
from wrappers.influxdb_helper import InfluxdbSource

from common.thread_utils import SmartThreadPoolExecutor

GITLAB_URL: str = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
GITLAB_PROJECT_PATH: str = os.environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
GITLAB_TOKEN: str = os.environ.get("GITLAB_TOKEN", "<token unspecified>")

DB_MEASUREMENT_MR: str = "merge-requests-stats"
DB_MEASUREMENT_PIPELINES: str = "streaks-pipelines-stats"
DB_MEASUREMENT_JOBS: str = "finished-jobs-stats"
DB_MEASUREMENT_UNIT_TESTS: str = "unit-tests-flakes-stats"

CONFIG_TRACES_FILE_PATH: str = "./mesa-ci-status/traces_config.yaml"

gl: GitlabConnector = GitlabConnector(GITLAB_PROJECT_PATH)
db_source: InfluxdbSource = InfluxdbSource("marge-stats")

lock: threading.Lock = threading.Lock()

with open(CONFIG_TRACES_FILE_PATH) as f:
    traces_config: Dict[str, Any] = yaml.load(f, Loader=yaml.FullLoader)


def get_failed_unit_tests_deqp_failures_csv(failures_csv: str) -> List[str]:
    if not failures_csv:
        return []

    failed_utests = []
    lines = failures_csv.splitlines()
    for line in lines:
        if test := line.split(",")[0]:
            failed_utests.append(test)
    return failed_utests


def get_failed_unit_tests_piglit_junit_xml(junit_xml: str) -> list[str]:
    if not junit_xml:
        return []

    root = ET.fromstring(junit_xml)
    failed_tests: list[str] = []

    for testcase in root.iter("testcase"):
        if testcase.get("status") == "fail":
            failed_tests.append(testcase.get("name", "None"))
        elif testcase.find("failure") is not None:
            failed_tests.append(testcase.get("name", "None"))

    return failed_tests


def get_failed_unit_tests_skqp(trace: str) -> List[str]:
    if not trace:
        return []

    # Get test names, exemples:
    # [0m2022-05-24 13:56:28.106391: Starting: gl_zero_control_stroke
    # [0m2022-05-24 13:58:35.054641: Starting test: unitTest_AbandonedContextImage  # noqa: E501
    pattern = r": Starting.*: *(.+)"
    tests = re.findall(pattern, trace)
    if not tests:
        return []

    # Get test status, examples:
    # [0m2022-05-24 13:56:07.475938: Passed:   gl_pathopsinverse
    # [0m2022-05-24 13:55:31.365298: FAILED:   gl_atlastext (19)
    # [0m2022-05-24 13:58:35.054677: Test passed:   unitTest_AbandonedContextImage  # noqa: E501
    # Get status from lines with the test name that doesn't have
    # "Starting" word
    tests_p = "|".join(tests)
    pattern = f": ((?!Starting).*): *({tests_p})"
    res = re.findall(pattern, trace)

    return [test for status, test in res if "passed" not in status.lower()]


def categorize_errors(trace: str) -> Dict[str, Any]:
    # sourcery skip: raise-specific-error
    for category in traces_config["categories"]:
        p = "|".join(category["patterns"])
        if re.findall(p, trace or ""):
            return category
    raise Exception(f"No category found, check {CONFIG_TRACES_FILE_PATH}")


def get_lava_stats(_gl: GitlabConnector, pjob: ProjectJob) -> Dict[str, Any]:
    lava_json = _gl.get_artifact(pjob, "results/lava_job_detail.json")
    if not lava_json:
        return {}
    try:
        lava = json.loads(lava_json)
    except json.JSONDecodeError as e:
        print("Error:", e)
        return {}

    return {
        "lava_fixed_tags": lava["fixed_tags"] or "None",
        "lava_dut_job_type": lava["dut_job_type"] or "None",
        "lava_job_combined_fail_reason": lava["job_combined_fail_reason"] or "None",
        "lava_job_combined_status": lava["job_combined_status"] or "None",
        "lava_dut_attempt_counter": lava["dut_attempt_counter"] or 0,
    }


def populate_influxdb_points_from_unit_tests(job: ProjectPipelineJob, user_project: str) -> None:
    if job.status != "failed":
        return

    _gl = gl if GITLAB_PROJECT_PATH == user_project else GitlabConnector(user_project)
    # We need jobs object from the project to get traces and other info
    pjob = _gl.project.jobs.get(job.id)

    trace = pjob.trace().decode("UTF-8")  # type: ignore
    junit_xml = _gl.get_artifact(pjob, "results/junit.xml")
    failures_csv = _gl.get_artifact(pjob, "results/failures.csv")

    failed_utests = get_failed_unit_tests_deqp_failures_csv(failures_csv)
    failed_utests += get_failed_unit_tests_piglit_junit_xml(junit_xml)
    failed_utests += get_failed_unit_tests_skqp(trace)

    for utest in failed_utests:
        point = {
            "measurement": DB_MEASUREMENT_UNIT_TESTS,
            "time": job.finished_at,
            "tags": {
                "job_name": job.name,
                "stage": job.stage,
                "unit_test": utest,
            },
            "fields": {
                "job_url": job.web_url,
            },
        }
        print(
            "\t\t\t",
            job.finished_at,
            "writting point to",
            DB_MEASUREMENT_UNIT_TESTS,
            "job",
            job.id,
        )
        with lock:
            db_source.write(Point.from_dict(point))


def get_failed_job_stats(pjob: ProjectJob) -> Dict[str, str]:
    if pjob.status != "failed":
        return {}
    trace = pjob.trace().decode("UTF-8")  # type: ignore
    category = categorize_errors(trace)
    return {
        "failure_category": category["name"],
        "failure_category_tag": category["tag"],
    }


def populate_influxdb_points_from_jobs(job: ProjectPipelineJob, user_project: str) -> None:
    _gl = gl if GITLAB_PROJECT_PATH == user_project else GitlabConnector(user_project)
    # We need the job object from the project to get traces and other info
    pjob = _gl.project.jobs.get(job.id)

    lava_stats = get_lava_stats(_gl, pjob)
    failed_job_stats = get_failed_job_stats(pjob)

    st = datetime.strptime(job.started_at, "%Y-%m-%dT%H:%M:%S.%fZ") if job.started_at else None
    queued_start_at = (
        (st - timedelta(seconds=job.queued_duration)).isoformat()
        if job.queued_duration and st
        else None
    )

    point = {
        "measurement": DB_MEASUREMENT_JOBS,
        "time": job.finished_at,
        "tags": {
            "job_name": job.name,
            "stage": job.stage,
            "failure_category": failed_job_stats.get("failure_category", "None"),
            "failure_category_tag": failed_job_stats.get("failure_category_tag", "None"),
            "status": job.status,
            "lava_dut_job_type": lava_stats.get("lava_dut_job_type", "None"),
            "lava_job_combined_fail_reason": lava_stats.get(
                "lava_job_combined_fail_reason", "None"
            ),
            "lava_job_combined_status": lava_stats.get("lava_job_combined_status", "None"),
        },
        "fields": {
            "job_url": job.web_url,
            "pipeline_url": job.pipeline.get("web_url", "None"),
            "queued_at": queued_start_at,
            "created_at": job.created_at,
            "finished_at": job.finished_at,
            "lava_fixed_tags": lava_stats.get("lava_fixed_tags", "None"),
            "lava_dut_attempt_counter": lava_stats.get("lava_dut_attempt_counter", 0),
        },
    }
    print(
        "\t\t",
        job.finished_at,
        "writing point to",
        DB_MEASUREMENT_JOBS,
        "job",
        job.id,
    )
    with lock:
        db_source.write(Point.from_dict(point))


def get_jobs_from_pipeline_id(user_project: str, pipeline_id: int) -> list[ProjectPipelineJob]:
    if not pipeline_id:
        return []
    _gl = gl if GITLAB_PROJECT_PATH == user_project else GitlabConnector(user_project)
    pipeline: ProjectPipeline = _gl.get_pipeline(pipeline_id)

    list_args = {
        "per_page": 100,
        "include_retried": True,
        "get_all": True,
    }
    return pipeline.jobs.list(**list_args)  # type: ignore


def populate_influxdb_points_from_pipeline(
    reason: str, user_project: str, jobs: List[ProjectPipelineJob]
) -> None:
    finished_jobs = [j for j in jobs if j.finished_at]

    populate_influxdb_jobs = partial(populate_influxdb_points_from_jobs, user_project=user_project)
    populate_influxdb_tests = partial(
        populate_influxdb_points_from_unit_tests, user_project=user_project
    )

    with SmartThreadPoolExecutor() as executor:
        list(executor.map(populate_influxdb_jobs, finished_jobs))
        list(executor.map(populate_influxdb_tests, finished_jobs))


def count_repeated_job_names(jobs: List[ProjectPipelineJob]) -> int:
    if not jobs:
        return 0

    job_names: dict[str, bool] = {}
    counter = 0
    for job in jobs:
        if not job_names.get(job.name):
            job_names[job.name] = True
        else:
            counter += 1
    return counter


def populate_influxdb_points_from_mr(mr: ProjectMergeRequest) -> None:
    list_args = {"per_page": 100, "iterator": True, "sort": "asc"}
    notes = mr.notes.list(**list_args)

    streaks_pipelines: list = []

    for note in notes:
        if note.body.startswith("added ") and note.author["username"] != "marge-bot":
            # new commits from author
            streaks_pipelines = []
        elif note.body.startswith("I couldn't merge"):

            def extract_info(s):
                # Regular expression to match the specific pattern
                pattern = re.compile(
                    r"I couldn't merge this branch: (?P<reason>.*?)(?= See pipeline|$)(?: See pipeline https://gitlab\.freedesktop\.org/(?P<user_project>[^/]+/[^/]+)/-/pipelines/(?P<pipeline>\d+))?"  # noqa: E501
                )
                match = pattern.search(s)
                if not match:
                    raise ValueError(f"Could not match the pattern from the note {s}")

                return (
                    match["reason"].strip(),
                    match["user_project"],
                    match["pipeline"],
                )

            reason, user_project, pipeline_id = extract_info(note.body)
            streaks_pipelines.append((reason, user_project, pipeline_id))

    point = {
        "measurement": DB_MEASUREMENT_MR,
        "time": mr.merged_at,
        "tags": {
            "merge_request_url": f"{GITLAB_URL}/{GITLAB_PROJECT_PATH}/-/merge_requests/{mr.iid}"  # noqa: E501
        },
        "fields": {"number_of_streaks_pipelines": len(streaks_pipelines)},
    }
    print(mr.merged_at, "writting point to", DB_MEASUREMENT_MR, "mr", mr.iid)
    with lock:
        db_source.write(Point.from_dict(point))

    # add a point per pipeline with the given reason
    for reason, user_project, pipeline_id in streaks_pipelines:
        user_project = user_project or GITLAB_PROJECT_PATH
        jobs = get_jobs_from_pipeline_id(user_project, pipeline_id)
        point = {
            "measurement": DB_MEASUREMENT_PIPELINES,
            "time": mr.merged_at,
            "tags": {
                "merge_request_url": f"{GITLAB_URL}/{user_project}/-/merge_requests/{mr.iid}",  # noqa: E501
                "failure_reason": reason,
            },
            "fields": {
                "pipeline_url": f"{GITLAB_URL}/{user_project}/-/pipelines/{pipeline_id}",  # noqa: E501
                "number_of_job_retries": count_repeated_job_names(jobs),
            },
        }
        print(
            "\t",
            mr.merged_at,
            "writting point to",
            DB_MEASUREMENT_PIPELINES,
            "pipeline",
            pipeline_id,
        )
        with lock:
            db_source.write(Point.from_dict(point))
        if jobs:
            populate_influxdb_points_from_pipeline(reason, user_project, jobs)


if __name__ == "__main__":
    since = db_source.get_last_write(
        measurement=DB_MEASUREMENT_MR,
        field="number_of_streaks_pipelines",
        range_start="-3mo",
    )

    print("Getting MRs merged after", since)

    merged_mrs = gl.mrs_in_main_merged_after(since)

    with SmartThreadPoolExecutor() as executor:
        list(executor.map(populate_influxdb_points_from_mr, merged_mrs))
