"""
This module contains utilities for threading.
"""

import concurrent.futures
import logging
import threading
import traceback
from functools import wraps
from typing import Any, Callable, Iterable, TypeVar

_T = TypeVar("_T")


def thread_exception_handler(func: Callable[..., None]):
    """Decorator for threads to catch exceptions and print them to the log."""

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> None:
        try:
            func(*args, **kwargs)
        except Exception as e:
            tb = traceback.format_exc()
            thread_name = threading.current_thread().name
            logging.error(f"Error in thread {thread_name}: {e} {args} {tb}")
            raise

    return wrapper


class SmartThreadPoolExecutor(concurrent.futures.ThreadPoolExecutor):
    """
    A wrapper around ThreadPoolExecutor that handles exceptions and timeouts.

    Args:
        max_workers (int): The maximum number of workers to use.

    Examples:
    1. No exception:
        >>> def square(x):
        ...     return x**2
        >>> with SmartThreadPoolExecutor() as executor:
        ...     results = list(executor.map(square, range(10)))
        >>> results
        [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
    2. Exception:
        >>> def divide(x):
        ...     return 1 / x
        >>> with SmartThreadPoolExecutor() as executor:
        ...     list(executor.map(divide, [0]))
        Traceback (most recent call last):
        ...
        ZeroDivisionError: division by zero
    3. Multiple arguments:
        >>> def pow_two_three(x, y):
        ...     return pow(x, y)
        >>> with SmartThreadPoolExecutor() as executor:
        ...     results = list(executor.map(pow_two_three, [2] * 10, range(10)))
        >>> results
        [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
    4. Timeout:
        >>> def long_running_job(x):
        ...     time.sleep(x/1000)
        >>> with SmartThreadPoolExecutor() as executor:
        ...     list(executor.map(long_running_job, range(100), timeout=.05))
        Traceback (most recent call last):
        ...
        concurrent.futures._base.TimeoutError
    """

    def __init__(self, max_workers=None):
        super().__init__(max_workers=max_workers)

    def map(
        self,  # type: ignore
        func: Callable[..., _T],
        *iterables: Iterable,
        timeout: float | None = None,
    ) -> Iterable[_T]:
        """Maps the function over the iterables with smart exception handling."""
        futures = [self.submit(func, *args) for args in zip(*iterables)]
        # use FIRST_EXCEPTION to raise the first exception that occurs
        done, pending = concurrent.futures.wait(
            futures, timeout=timeout, return_when=concurrent.futures.FIRST_EXCEPTION
        )

        # print the first exception that occurred
        for f in done:
            if not f.cancelled() and f.exception() is not None:
                raise f.exception()  # type: ignore

        # if it timed out, cancel the pending futures
        if pending:
            for not_done in pending:
                print(not_done)
                not_done.cancel()
            self.shutdown(wait=False)  # Don't wait, they may be deadlocked.
            raise TimeoutError()

        yield from (f.result() for f in done)
