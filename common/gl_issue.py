import os
import smtplib
import sys
from email.mime.text import MIMEText

SMTP_SERVER = os.environ.get("SMTP_SERVER", "<server unspecified>")
SMTP_SERVER_PORT = os.environ.get("SMTP_SERVER_PORT", 587)
SMTP_USER = os.environ.get("SMTP_USER", "<user unspecified>")
SMTP_PASSWD = os.environ.get("SMTP_PASSWD", "<passwd unspecified>")
SMTP_EMAIL = os.environ.get("SMTP_EMAIL", "<email unspecified>")


def gl_issue_create_report(project, description, title, label):
    issue = project.issues.create({"title": title, "description": description})
    issue.labels = [label]
    issue.save()
    return issue


def gl_issue_get_report(project, created_after, label, title):
    list_args = {
        "created_after": f"{created_after}T00:00:00.000Z",
        "labels": [label],
        "sort": "desc",
        "state": "opened",
        "search": title,
    }

    issues = project.issues.list(**list_args)
    return issues[0] if issues else None


def gl_issue_post_comment(issue, comment):
    note = issue.notes.create({"body": comment})
    note.save()
    return note


def gl_send_report(project, report, title, new_report_date, label, description=None) -> bool:
    has_created_issue = False
    issue = gl_issue_get_report(project, new_report_date, label, title)
    if issue:
        gl_issue_post_comment(issue, report)
    elif description:
        issue = gl_issue_create_report(project, description, title, label)
        gl_issue_post_comment(issue, report)
        has_created_issue = True
    else:
        issue = gl_issue_create_report(project, report, title, label)
        has_created_issue = True
    print(f"Report posted on {issue.web_url}", file=sys.stderr)
    return has_created_issue


def send_email(email_to, report, subject):
    if not email_to:
        return
    # Create the container (outer) email message.
    msg = MIMEText(report)
    msg["Subject"] = subject
    msg["From"] = SMTP_EMAIL
    msg["To"] = email_to

    print(f"Sending report by email from {msg['From']} to {msg['To']}", file=sys.stderr)

    s = smtplib.SMTP(SMTP_SERVER, SMTP_SERVER_PORT)
    s.starttls()
    s.login(SMTP_USER, SMTP_PASSWD)
    s.sendmail(SMTP_EMAIL, email_to.split(","), msg.as_string())
    s.quit()
