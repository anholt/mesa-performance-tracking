git+https://github.com/jakubplichta/grafana-dashboard-builder@66952ad0380180fda7fd187d88089d2c9fc36009
influxdb_client==1.36.1
pytest==7.2.1
python-gitlab==3.13.0

-r requirements-lint.txt
-r alerts/requirements.txt
-r dashboard_exporter/requirements.txt
-r mesa-ci-status/requirements.txt
